/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iaproyect;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JFrame;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import java.awt.Color;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 * @author eygonzalez
 */
public class IAProyect extends JFrame implements KeyListener{

    JPanel p = new JPanel();
    JTextArea dialog = new JTextArea(20,50);
    JTextArea input = new JTextArea(1,50);
    JScrollPane scroll = new JScrollPane(
        dialog,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    
    String h = "Las hamburguesas son deliciosas";
    String c = "Las hamburguesas son costosas";
    String f = "Las flores huelen bien";
    String b = "El cuarto huele bien";
    String m = "Maria se siente alegre";
    String v = "El viernes es dia de rumba";
    String u = "La salida de la UECCI es mas temprano";
    String ant;
    
    public static void main(String[] args) {
        new IAProyect();
    }
    
    public IAProyect(){
        super ("INTELIGENCIA ARTIFICIAL");
        setSize(600,400);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        dialog.setEditable(false);
        input.addKeyListener(this);
        
        p.add(scroll);
        p.add(input);
        p.setBackground(Color.GREEN);
        add(p);
        
        setVisible(true);
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER){
            input.setEditable(false);
            
            //------Grabar Texto-------
            String texto = input.getText();
            input.setText("");
            addText("--> Tu:\t" + texto);
            texto.trim();
            
            while (
                    texto.charAt(texto.length() -1) == '!' ||
                    texto.charAt(texto.length() -1) == '?' ||
                    texto.charAt(texto.length() -1) == '.' ||
                    texto.charAt(texto.length() -1) == ','
                    ){
                texto = texto.substring(0, texto.length() -1);
            }
            texto.trim();

            if (texto.length() == h.length()){
                addText("\n--> Armani:\t" +c);
                ant=c;
            }
            
            if ((texto.toLowerCase()).length() == (f.toLowerCase()).length()){
                addText("\n--> Armani:\t" +b);
                ant = b;
                System.out.println(ant);
                if ((ant.toLowerCase().length()) == (b.toLowerCase().length())){
                    addText("\n--> Armani:\t" +m);
                }
            }
            
            if ((texto.toLowerCase()).length() == (b.toLowerCase()).length()) {
                addText("\n--> Armani:\t" +m);
            }
            if ((texto.toLowerCase().length() == (v.toLowerCase()).length())){
                addText("\n--> Armani:\t" +u);
            }
            
            /*else {
                addText("\n--> Armani:\t" + "(Armani no puede responderte)");
                System.out.println(texto.toLowerCase());
                System.out.println(f.toLowerCase());
            }*/
            
            addText("\n");
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER){
            input.setEditable(true);
        }
    }
    
    public void keyTyped(KeyEvent e) {
        
    }
    
    public void addText(String Str){
        dialog.setText(dialog.getText() + Str);
    }  
}

